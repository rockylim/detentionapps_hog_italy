local initScore, setScore;

--local scoreFX = audio.loadSound("SOUNDS/score_incr1.mp3");

initScore = function(x, y, width, height, num, numTab, group, offSet)
	local new = display.newGroup();
	group:insert(new);
	new.numTab = {};
	local xnew = 0;
	for i = 1, num do
		local grupNum = display.newGroup();
		new:insert(grupNum);
		new.numTab[#new.numTab+1] = grupNum;
		for i = 1, 10 do
			local num = display.newImageRect(grupNum, numTab[i], width, height);
			num.isVisible = false;
			num.x = xnew
		end
		grupNum.num = grupNum[1];
		if not offSet then
		offSet = 0;
		end
		xnew = -width+offSet+xnew;
	end
	new.x, new.y = x, y;
	new.points = 0;
	setScore( 0, new);
	return new;
end

setScore = function(points, numgroup)
--	audio.play(scoreFX, {channel = audio.findFreeChannel()});
	local div = 1;
	local newDiv = 10;
	local numbers = math.floor(math.log10(points))+1;
	numgroupTab = numgroup.numTab;
	if (points < 1) then
		numbers = 1;
		for i = 1, #numgroupTab do
			numgroupTab[i].num.isVisible = false;
			if numgroupTab[i].num ~= numgroupTab[i][1] then
				numgroupTab[i].parent:insert(numgroupTab[i]);
				transition.to(numgroupTab[i], {time = 100, xScale = 1.5, yScale = 1.5});
				transition.to(numgroupTab[i], {delay = 100, time = 200, xScale = 1, yScale = 1});
			end
			numgroupTab[i][1].isVisible = true;
			numgroupTab[i].num = numgroupTab[i][1];
		end
	--	numgroup[1].num.isVisible = false;
	--	numgroup[1][numbers].isVisible = true;
		numgroup.points = points;
		return;
	end
	for i = 1, numbers do
		local img = math.floor((points%newDiv)/div)+1;
		numgroupTab[i].num.isVisible = false;
		if numgroupTab[i].num ~= numgroupTab[i][img] then
			numgroupTab[i].parent:insert(numgroupTab[i]);
			transition.to(numgroupTab[i], {time = 100, xScale = 1.5, yScale = 1.5});
			transition.to(numgroupTab[i], {delay = 100, time = 200, xScale = 1, yScale = 1});
		end
		numgroupTab[i][img].isVisible = true;
		numgroupTab[i].num = numgroupTab[i][img];
		div = div*10;
		newDiv = newDiv*10;
	end
	--local numLeft = numgroup.numChildren-numbers;
    for i = numbers+1, #numgroupTab do
		if numgroupTab[i] then
			if numgroupTab[i].num then
				numgroupTab[i].num.isVisible = false;
				numgroupTab[i][1].isVisible = true;
				if numgroupTab[i].num ~= numgroupTab[i][1] then
					numgroupTab[i].parent:insert(numgroupTab[i]);
					transition.to(numgroupTab[i], {time = 100, xScale = 1.5, yScale = 1.5});
					transition.to(numgroupTab[i], {delay = 100, time = 200, xScale = 1, yScale = 1});
				end
				numgroupTab[i].num = numgroupTab[i][1];
			end
		end
	end
	numgroup.points = points;
end

scoreFun = {initScore = initScore, setScore = setScore};

return (scoreFun);