module(..., package.seeall);

local _W = display.contentWidth;
local _H = display.contentHeight;
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 1136);
local yScale = (_H / 768);
local group;
local image;
local rectWidth = 450;

local function onTap(event)
  if (event.phase == "ended") then        
  	hide();  	
  end    
  return true;
end

function initialize(parent)
	group = display.newGroup();
	hide();

	group.x = _w;
	group.y = _h;
	parent:insert(group);
	
	local rectHalfWidth = rectWidth * 0.5;

	local borderWidth = 10;
	local borderHalfWidth = borderWidth * 0.5;

	local blackScreen = display.newRect( 0, 0, _W, _H );
	blackScreen:setFillColor(0,0,0);
	blackScreen.alpha = 0.6;
	group:insert(blackScreen);
	blackScreen:addEventListener("touch", onTap);

	local contentRect = display.newRect( 0, 0, rectWidth, rectWidth ); 
	contentRect:setFillColor(0,0,0);
	contentRect.alpha = 0.9;
	group:insert(contentRect);	

	local topLine = display.newRect(0, -rectHalfWidth, rectWidth, borderWidth);
	topLine:setFillColor(0.203, 0.596, 0.858);
	group:insert(topLine);

	local bottomLine = display.newRect(0, rectHalfWidth, rectWidth, borderWidth);
	bottomLine:setFillColor(0.203, 0.596, 0.858);
	group:insert(bottomLine);

	local leftLine = display.newRect(0,0, borderWidth, rectWidth);
	leftLine:setFillColor(0.203, 0.596, 0.858);
	leftLine.y = 0;
	leftLine.x = -rectHalfWidth + borderHalfWidth;
	group:insert(leftLine);

	local rightLine = display.newRect(0,0, borderWidth, rectWidth);
	rightLine:setFillColor(0.203, 0.596, 0.858);
	rightLine.y = 0;
	rightLine.x = rectHalfWidth - borderHalfWidth;
	group:insert(rightLine);

	image = display.newGroup();
	image.x = 0;image.y = 0;
	group:insert(image);

	local closeButton = display.newImageRect("assets/images/btn_close.png", 55, 55);	
	closeButton.y = -rectHalfWidth + 10;
	closeButton.x = rectHalfWidth - 10;
	group:insert(closeButton);	
end


function destroy()  
	display.remove(group);
	group = nil;
end

function show(fileName, width, height) 	
	display.remove(image);
	image = display.newImage(fileName);
	image.x = 0;image.y = 0;
	image.width = width;
	image.height = height;
	if (width > height) and (width > rectWidth) then
		local scale = (rectWidth - 100) / width;
		image.width = width * scale;
		image.height = height * scale;		
	end;
	if (height > width) and (height > rectWidth) then
		local scale = (rectWidth - 100) / height;
		image.width = width * scale;
		image.height = height * scale;		
	end;
	group:insert(image);
	group.alpha = 1;
end

function hide() 
	group.alpha = 0;
end