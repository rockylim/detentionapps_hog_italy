module(..., package.seeall);

local gameNetwork = require("gameNetwork");

--supply name of leaderboard specified on back end
local LEADERBOARD_NAME = "";

local isInitialized;

local openGameCenter = nil;
local gameCenterDismissed = nil;
local submitScore = nil;
local openAchievements = nil;

local function initCallback( event )
    -- "showSignIn" is only available on iOS 6+
    if event.type == "showSignIn" then
        -- This is an opportunity to pause your game or do other things you might need to do while the Game Center Sign-In controller is up.
        -- For the iOS 6.0 landscape orientation bug, this is an opportunity to remove native objects so they won't rotate.
    -- This is type "init" for all versions of Game Center.
    elseif event.data then
        -- loggedIntoGC = true
        -- native.showAlert( "Success!", "", { "OK" } )
    end
end

function initialize()
  --if we somehow are trying to re-initialize, don't
  if (isInitialized) then return; end
  
  isInitialized = true;
  
	gameNetwork.init( "gamecenter", initCallback);
	
	Runtime:addEventListener("openGameCenter", openGameCenter);
	Runtime:addEventListener("submitScore", submitScore);
  Runtime:addEventListener("openAchievements", openAchievements);

end

openAchievements = function(e)
  gameNetwork.show("achievements");
end

openGameCenter = function(e)
	gameNetwork.show( "leaderboards", { leaderboard = {category=LEADERBOARD_NAME}, listener=gameCenterDismissed } )	
end

gameCenterDismissed = function(e)
	print("Game Center closed.");
end

-- Called after a request has been sent.
local function onGameNetworkRequestResult( e )
    if (e.type == "setHighScore") then
        -- High score has been set.
        print("High Score successfully sent to GameCenter");
    end
end

--e.score to access score
submitScore = function(e)
	print("Sending score of " .. tostring(e.score) .. " to GameCenter");
  local score = (e.score or 0);
  
	gameNetwork.request( "setHighScore",
	{
	    localPlayerScore = { category = LEADERBOARD_NAME, value = math.floor(score) },
	    listener = onGameNetworkRequestResult,
	})
end