module(..., package.seeall);

local SOUNDS_DIRECTORY = "assets/sound/";

local SOUND_CHANNELS = {};
SOUND_CHANNELS[1] = 1;
SOUND_CHANNELS[2] = 2;
SOUND_CHANNELS[3] = 3;
SOUND_CHANNELS[4] = 4;
SOUND_CHANNELS[5] = 5;
SOUND_CHANNELS[6] = 6;

local MUSIC_CHANNEL = 10;

local rand = math.random;
local randSeed = math.randomseed;
local randTime = os.time;
rand(); rand(); rand();

local muted;

local currentSong;

local soundData = 
{
  button_sound = 
  {
    audio.loadSound(SOUNDS_DIRECTORY .. "button.mp3"),
  },

  wrong_sound = 
  {
    audio.loadSound(SOUNDS_DIRECTORY .. "wrong.mp3"),
  },
  
  zoom_sound = 
  {
    audio.loadSound(SOUNDS_DIRECTORY .. "zoom.mp3"),
  },
  
  hint_sound =
  {
    audio.loadSound(SOUNDS_DIRECTORY .. "hint.mp3"),
  },
  
  item_sound =
  {
    audio.loadSound(SOUNDS_DIRECTORY .. "item_found1.mp3"),
    audio.loadSound(SOUNDS_DIRECTORY .. "item_found2.mp3"),
    audio.loadSound(SOUNDS_DIRECTORY .. "item_found3.mp3"),
    audio.loadSound(SOUNDS_DIRECTORY .. "item_found4.mp3"),
  },
  
  bonus_item_sound = 
  {
    audio.loadSound(SOUNDS_DIRECTORY .. "bonus_item_found.mp3"),
  },  
};

local musicData = 
{
  menu_music = audio.loadSound(SOUNDS_DIRECTORY .. "menu_music.mp3"),
--  gameplay_music = audio.loadSound(SOUNDS_DIRECTORY .. "gameplay.mp3"),
};

local soundMuted;
local musicMuted;

local function playSound(strName)
  local soundSection = soundData[strName];
  
  print(strName, #soundSection);
  local randSound = rand(1, #soundSection);
  
  for i = 1, #SOUND_CHANNELS do
    if (audio.isChannelActive(SOUND_CHANNELS[i]) == false) then
      audio.play(soundSection[randSound], {channel = SOUND_CHANNELS[i]}); 
      return;
    end
  end
end

local function playMusic(strName)
  if (currentSong == strName) then return; end
  
  currentSong = strName;
  
	audio.stop(MUSIC_CHANNEL);
	audio.play(musicData[strName], {loops = -1, channel = MUSIC_CHANNEL});
end

local function mute(e)
	muted = true;
	audio.stop();
end

local function unmute(e)
	muted = false;
end

local function toggleMuteSound(e)
  if (e.toggle == "off") then
    for i = 1, #SOUND_CHANNELS do
      audio.setVolume(0,{channel = SOUND_CHANNELS[i]});
    end
    _G.SOUND_MUTED = true;
  elseif (e.toggle == "on") then
    for i = 1, #SOUND_CHANNELS do
      audio.setVolume(1,{channel = SOUND_CHANNELS[i]});
    end
    Runtime:dispatchEvent({name = "button_sound"});
    _G.SOUND_MUTED = false;
  end
  
end

local function toggleMuteMusic(e)
  Runtime:dispatchEvent({name = "button_sound"});
  
  if (e.toggle == "off") then
    audio.setVolume(0,{channel = MUSIC_CHANNEL});
    _G.MUSIC_MUTED = true;
  elseif (e.toggle == "on") then
    audio.setVolume(1,{channel = MUSIC_CHANNEL});
    _G.MUSIC_MUTED = false;
  end
end

function initialize()
  if (_testMode == false) then
    muted = false;
  else
    muted = false;
  end
	
	for i,v in pairs(soundData) do
		Runtime:addEventListener(i, function(e)
      playSound(i);
		end);
	end
	
	for i,v in pairs(musicData) do
		Runtime:addEventListener(i, function(e)
			playMusic(i);
		end);
	end
	
	Runtime:addEventListener("button_sound", toggleMuteSound);
	Runtime:addEventListener("button_music", toggleMuteMusic);
end
