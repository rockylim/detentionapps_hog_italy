module(..., package.seeall);

--publisher id
local APP_ID = _G.ADMOB_INTERSTITIALS_ID;
local BANNER_ID = _G.ADMOB_BANNER_ID;

local TIME_TIL_AUTO_CLOSE = 6000;

local admob;

local handleCloseAd;

--forward declaration
local showInterstitial = nil;
local showBannerAd = nil;
local hideAd = nil;
local stopAds = nil;
local loadIntAd = nil;

local function adListener(e)
	print("Result of AdMob Call : " .. tostring(e.response));
end

local function getIfUnlocked()
  local fileIO = require("utility.fileParsing");
  
  local userData = fileIO.getUserData();
  
  return (userData.game_unlocked.unlocked or false);
end

function initialize()
--  if ((_G.FREE_VERSION == false) and (_G.GAME_UNLOCKED == true)) then return; end
  
  if (getIfUnlocked() == true) then return; end
  
  _G.ADS_INITIALIZED = true;
	admob = require("ads");
	admob.init("admob", BANNER_ID, adListener);
	Runtime:addEventListener("showInterstitial", showInterstitial);
	Runtime:addEventListener("showBannerAd", showBannerAd);
	Runtime:addEventListener("hideAd", hideAd);
	Runtime:addEventListener("no_ads", stopAds);
  Runtime:addEventListener("loadIntAd", loadIntAd);
end

loadIntAd = function(e)
--  if (_G.playerData.purchased == 1) then return; end
  if (getIfUnlocked() == true) then return; end
  
--  pcall(function()
--      if (admobInterstitial.isLoaded("interstitial") == false) then
--        admobInterstitial.load("interstitial", {appId=APP_ID, testMode=false});
--      end
--  end);
end

showInterstitial = function(e)
  if (getIfUnlocked() == true) then return; end
  
--	admob.show("interstitial", {x=0, y=0,appId = APP_ID});
  
--  handleCloseAd = timer.performWithDelay(TIME_TIL_AUTO_CLOSE, function(e)
--    hideAd();
--    Runtime:dispatchEvent({name = "closedInterstitial"});
--  end);
end

showBannerAd = function(e)
--  if (getIfUnlocked() == true) then return; end
  
--  local adY = 0;
  
--  if (e.isBottom == true) then
--    adY = (display.contentHeight - display.screenOriginY);
--  end
  
----	local adY = (display.contentHeight - display.screenOriginY);
--	admob.show("banner", {x=0, y=adY,appId = BANNER_ID});
end

hideAd = function(e)
	admob.hide();
end

stopAds = function(e)
  Runtime:removeEventListener("showInterstitial", showInterstitial);
	Runtime:removeEventListener("showBannerAd", showBannerAd);
	Runtime:removeEventListener("hideAd", hideAd);
	Runtime:removeEventListener("no_ads", stopAds);
  Runtime:removeEventListener("loadIntAd", loadIntAd);
  hideAd();
end
