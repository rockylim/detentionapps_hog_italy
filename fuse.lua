module(..., package.seeall);

--publisher id
local INTERSTITIALS_APP_ID = _G.FUSE_INT_ID;

--local BANNERS_APP_ID = _G.BANNERS_APP_ID;

local TIME_TIL_AUTO_CLOSE = 6000;

--local admobInterstitial;
--local admobBanner;

local fuse;

local handleCloseAd;

--forward declaration
local showInterstitial = nil;
local showBannerAd = nil;
local hideAd = nil;
local stopAds = nil;

local function adListener(e)
	print("Result of fuse Call : " .. tostring(e.response));
--  native.showAlert("Debug", tostring(e.response), {"OK"});
end

local function getAdsDisabled()
  local fileIO = require("utility.fileParsing");
  local data = fileIO.getUserData();
  
  return (data.game_unlocked.unlocked == true);
end

function initialize()
  if (getAdsDisabled() == true) then return; end
  
  _G.ADS_INITIALIZED = true;
  
	fuse = require("plugin.fuse");
  
  fuse.init(INTERSTITIALS_APP_ID,adListener);
  
--  native.showAlert("TEST", "AD INITED", {"OK"});
  
  --TEST
	Runtime:addEventListener("showInterstitial", showInterstitial);
	Runtime:addEventListener("no_ads", stopAds);
  
  
  
end

showInterstitial = function(e)
  if (getAdsDisabled() == true) then return; end
  
  fuse.show();
  
--	admobInterstitial.show("interstitial", {x=0, y=0,appId=INTERSTITIALS_APP_ID});
  
--  timer.performWithDelay(TIME_TIL_AUTO_CLOSE, function()
--    Runtime:dispatchEvent({name = "closedInterstitial"});
--    hideAd();
--  end);
end

stopAds = function(e)
  Runtime:removeEventListener("showInterstitial", showInterstitial);
	
	Runtime:removeEventListener("no_ads", stopAds);
end
