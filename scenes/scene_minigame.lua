local storyboard = require("storyboard");
storyboard.purgeOnSceneChange = true;
local scene = storyboard.newScene();

local _W = display.contentWidth;
local _H = display.contentHeight;
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 1136);
local yScale = (_H / 768);

--contains all visuals
local children;

local listImages;

local nextScene;

-- Called when the scene's view does not exist:
function scene:createScene( event )
	print("creating menu scene");
	listImages = {};
	
	children = self.view;
	
--	local assetManager = require("utility.assetManager");
--	listImages = assetManager.getAssets("menu/menu", children);
  
  --nextScene = storyboard.loadScene("scenes.scene_modes", false);

-- Do something here

-- Later, transition to the scene (no loading necessary)
--storyboard.gotoScene( "scene2", "slideLeft", 800 )
end


-- Called BEFORE scene has moved onscreen:
function scene:willEnterScene( event )
	
end

local function playMenuMusic(e)
  Runtime:dispatchEvent({name = "menu_music"});
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
  
end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )  
--  if (_G.GAME_UNLOCKED == false) then
--    Runtime:removeEventListener("levels_unlocked", handleUnlock);
--  end
  
--  Runtime:removeEventListener("background_hit", backgroundHit);
--  Runtime:removeEventListener("more_games_hit", moreGamesHit);
--  Runtime:removeEventListener("restore_purchases_hit", restoreHit);
--  Runtime:removeEventListener("purchase_unlock_hit", purchaseUnlockHit);
  
--  Runtime:dispatchEvent({name = "showInterstitial"});
  --native.setActivityIndicator(true);
end


-- Called AFTER scene has finished moving offscreen:
function scene:didExitScene( event )
	
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	
end


-- Called if/when overlay scene is displayed via storyboard.showOverlay()
function scene:overlayBegan( event )
	
end


-- Called if/when overlay scene is hidden/removed via storyboard.hideOverlay()
function scene:overlayEnded( event )
	
end

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "willEnterScene" event is dispatched before scene transition begins
scene:addEventListener( "willEnterScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "didExitScene" event is dispatched after scene has finished transitioning out
scene:addEventListener( "didExitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-- "overlayBegan" event is dispatched when an overlay scene is shown
scene:addEventListener( "overlayBegan", scene )

-- "overlayEnded" event is dispatched when an overlay scene is hidden/removed
scene:addEventListener( "overlayEnded", scene )

---------------------------------------------------------------------------------

return scene;