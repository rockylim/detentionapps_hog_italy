
-- config.lua

application =
{
    license =
    {
        google =
        {
            --put google key between the quotes below: key is obtained from the Google Play Developers Console under All applications>Your App>Services & APIs>LICENSING & IN-APP BILLING (note that this key can be fairly big)
            key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAm64adHmTFPM62UutRBWhRJ01EroPZ5Gi7+UgEeXyTF266AjNezLxLLddb0RXSX3cHvwNjvJDfuHNHntVde51ixcDijikVhZY2cJlrwrBsAmlZAu3lSIfysuhcNP7L+yUwJFK2CQjtS5ZtEvJxaek5Dsg3gBNEe1vAEuMqCntRljHgFGABG2rFL1peyPgDScLRKt2Rul86PwkxtvYeNEki89Y13Vmpzcc5bvh132ilDAUIrST/dEG4a2kaghj2ZU8u/OuV59K93dF12Bc9udD8PSDezz1x6Z+w3c/ymp6OI2RzGRtU8MfPI48VRP0Jb+9JyhRMDEvi3SzBxgfCcZEZwIDAQAB",
        },
    },
    content =
    {
           -- graphicsCompatibility = 1,  --this turns on V1 Compatibility mode
            width = 768,
            height = 1136,
            scale = "zoomStretch",
            fps=30,
			
			imageSuffix =
			{
				--["@2x"] = 1.5,
				--["@4x"] = 4,
			},
    },
}


