module(..., package.seeall);

--publisher id
local APP_ID;
local BANNER_ID;

local TIME_TIL_AUTO_CLOSE = 6000;

local applovin;

local handleCloseAd;

local currentBannerY;
local showIntOnLoad;
local showBannerOnLoad;

local function adListener(e)
	print("Result of Applovin Call : " .. tostring(e.response) .. " " .. tostring(e.phase));
  if ( e.phase == "init" ) then  -- Successful initialization
      print( e.provider )
      applovin.load("interstitial");
      
  elseif (e.phase == "loaded") then
    if (showIntOnLoad == true) then
      showIntOnLoad = nil;
      applovin.show();
    end
    
--    "banner" and "interstitial".
  elseif ((e.phase == "displayed")) then
    print(" " .. e.type);
    applovin.load("interstitial");
    Runtime:dispatchEvent({name = "adShown"});
    
  elseif (e.phase == "failed") then
    showIntOnLoad = nil;
    applovin.load("interstitial");
    Runtime:dispatchEvent({name = "adFailed"});
  end
  
  print("AD RESPONSES");
  print(tostring(e.type), tostring(e.phase));
end

--local function getIfUnlocked()
--  local fileIO = require("utility.fileParsing");
--  local data = fileIO.getUserData();
  
--  return (data.game_unlocked.unlocked == true);
--end
 
function initialize(intID, bannerID)
--  if (getIfUnlocked() == true) then return; end
  
  APP_ID = intID;
  BANNER_ID = bannerID;
  
	applovin = require( "plugin.applovin.paid" );
  
  applovin.init( adListener, { sdkKey=APP_ID,} )
  
--	Runtime:addEventListener("showInterstitial", showInterstitial);
--	Runtime:addEventListener("showBannerAd", showBannerAd);
--	Runtime:addEventListener("hideAd", hideAd);
--	Runtime:addEventListener("no_ads", stopAds);
--  Runtime:addEventListener("loadIntAd", loadIntAd);
end

--loadIntAd = function(e)
function loadIntAd(e)
end

--showInterstitial = function(e)
function showInterstitial()
--  if (getIfUnlocked() == true) then return; end
  
  showIntOnLoad = true;
  
	if ( applovin.isLoaded("interstitial") ) then
    showIntOnLoad = nil;
    applovin.show( "interstitial" );
    return;
  else
    applovin.load( "interstitial", { adUnitId=APP_ID, childSafe=true } )
  end
end

--hideAd = function(e)
function hideAd()
--	applovin.hide();
end

stopAds = function(e)
--  Runtime:removeEventListener("showInterstitial", showInterstitial);
--	Runtime:removeEventListener("showBannerAd", showBannerAd);
--	Runtime:removeEventListener("hideAd", hideAd);
--	Runtime:removeEventListener("no_ads", stopAds);
--  Runtime:removeEventListener("loadIntAd", loadIntAd);
--  hideAd();
end
