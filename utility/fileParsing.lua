module(..., package.seeall);

local SHOP_DATA_FILE = "userShopData.json";
local SHOP_FILE_LOCATION = system.DocumentsDirectory;

local USER_DATA_FILE = "userData.json";
local USER_FILE_LOCATION = system.DocumentsDirectory;

local json = require("json");

local function readFile(strFilename, directoryType)
    local filePath = system.pathForFile( strFilename, directoryType)
	
    local file;
    local contents = nil;
    
--    print(filePath);
    
    --filePath will be 'nil' if file doesn,t exist
    if (filePath) then
        file = io.open( filePath, "r");
        contents = file:read("*a");
        
--        print(contents);
--        print(json.decode(contents));
    end
    
    if (contents) then
        
        io.close(file);
        return json.decode(contents);
    end
end

local function writeFile(data, strFilename, directoryType)
  local path = system.pathForFile( strFilename, system.DocumentsDirectory)
	
  local file = io.open(path, "w")	
  if file then
      local contents = json.encode(data);
      file:write(contents);
      io.close(file);
  else --failed
    
  end
end

function loadData(strFilename, directoryType)
	return readFile(strFilename, directoryType or system.DocumentsDirectory);
end

function saveData(data, strFilename, directoryType)
  writeFile(data, strFilename, directoryType);
end

function getIfFileExists(strFilename, directoryType)
  local filePath = system.pathForFile(strFilename, directoryType);
	
  if (filePath) then
      local fileHandle = io.open( filePath, "r" )
      if (fileHandle) then
        io.close(fileHandle)
        return true;
      end
    
  end
  
  return false;
end

local function deleteFile(filename, dir)
  os.remove(system.pathForFile(filename, dir)); 
end

--shop methods
function getUserData()
  if (getIfStoreDataExists() == false) then
    cacheInitialStoreData(FREE_VERSION);
  end
  
  return getStoreData();
end

function deleteUserData()
  if (getIfStoreDataExists() == true) then
    deleteFile(SHOP_DATA_FILE, SHOP_FILE_LOCATION);
  end
end

function getIfStoreDataExists()
    return getIfFileExists(SHOP_DATA_FILE, SHOP_FILE_LOCATION);
end

function getStoreData()
  return loadData(SHOP_DATA_FILE, SHOP_FILE_LOCATION);
end

function cacheHighScore(num)
  local data = getUserData();
  data.high_score = num;
  saveShopData(data);
end

function cacheInitialStoreData(bIsUnlocked)
  local newUserData = 
  {
    {name = "game_unlocked", unlocked = bIsUnlocked},
    {name = "no_ads", unlocked = bIsUnlocked},
    {name = "levels", data =
      {
        {name = "traditional1picture", highScore = 0},
        {name = "traditional1word", highScore = 0},
        {name = "traditional1collector", highScore = 0},
        {name = "traditional2picture", highScore = 0},
        {name = "traditional2word", highScore = 0},
        {name = "traditional2collector", highScore = 0},
        {name = "traditional3picture", highScore = 0},
        {name = "traditional3word", highScore = 0},
        {name = "traditional3collector", highScore = 0},
        {name = "traditional4picture", highScore = 0},
        {name = "traditional4word", highScore = 0},
        {name = "traditional4collector", highScore = 0},
        {name = "traditional5picture", highScore = 0},
        {name = "traditional5word", highScore = 0},
        {name = "traditional5collector", highScore = 0},
        {name = "traditional6picture", highScore = 0},
        {name = "traditional6word", highScore = 0},
        {name = "traditional6collector", highScore = 0},
        {name = "traditional7picture", highScore = 0},
        {name = "traditional7word", highScore = 0},
        {name = "traditional7collector", highScore = 0},
        {name = "traditional8picture", highScore = 0},
        {name = "traditional8word", highScore = 0},
        {name = "traditional8collector", highScore = 0},
        
        {name = "chill1picture", highScore = 0},
        {name = "chill1word", highScore = 0},
        {name = "chill1collector", highScore = 0},
        {name = "chill2picture", highScore = 0},
        {name = "chill2word", highScore = 0},
        {name = "chill2collector", highScore = 0},
        {name = "chill3picture", highScore = 0},
        {name = "chill3word", highScore = 0},
        {name = "chill3collector", highScore = 0},
        {name = "chill4picture", highScore = 0},
        {name = "chill4word", highScore = 0},
        {name = "chill4collector", highScore = 0},
        {name = "chill5picture", highScore = 0},
        {name = "chill5word", highScore = 0},
        {name = "chill5collector", highScore = 0},
        {name = "chill6picture", highScore = 0},
        {name = "chill6word", highScore = 0},
        {name = "chill6collector", highScore = 0},
        {name = "chill7picture", highScore = 0},
        {name = "chill7word", highScore = 0},
        {name = "chill7collector", highScore = 0},
        {name = "chill8picture", highScore = 0},
        {name = "chill8word", highScore = 0},
        {name = "chill8collector", highScore = 0},
        
        {name = "adventure1", highScore = 0},
        {name = "adventure2", highScore = 0},
        {name = "adventure3", highScore = 0},
        {name = "adventure4", highScore = 0},
        {name = "adventure5", highScore = 0},
        {name = "adventure6", highScore = 0},
        {name = "adventure7", highScore = 0},
        {name = "adventure8", highScore = 0},
        {name = "adventure9", highScore = 0},
        {name = "adventure10", highScore = 0},
        {name = "adventure11", highScore = 0},
        {name = "adventure12", highScore = 0},
        {name = "adventure13", highScore = 0},
        {name = "adventure14", highScore = 0},
        {name = "adventure15", highScore = 0},
      }
    }
    
  };
  
  local toCacheData = {};
  
  for i = 1, #newUserData do
    local data = newUserData[i];
    toCacheData[data.name] = {};
    toCacheData[data.name].unlocked = data.unlocked;
  end
  
  --cache high scores / is complete
  for i = 1, #newUserData[3].data do
    local data = newUserData[3].data[i];
    toCacheData[data.name] = {};
    toCacheData[data.name].highScore = data.highScore;
    toCacheData[data.name].isCompleted = (data.isCompleted or false);
    print("wurf");
  end
  
  print("shmork");
  saveData(toCacheData, SHOP_DATA_FILE, SHOP_FILE_LOCATION);

  
--  Runtime:dispatchEvent({name = "restore_purchases"});
end

function saveShopData(data)
  saveData(data, SHOP_DATA_FILE, SHOP_FILE_LOCATION);
end
--end shop methods

function cacheSoundSettings(musicVolume, soundVolume)
  local userData = loadData(USER_DATA_FILE, USER_FILE_LOCATION);
  userData["music_volume"] = musicVolume;
  userData["sound_volume"] = soundVolume;
  saveData(userData, USER_DATA_FILE, USER_FILE_LOCATION);
end

function getAudioSettings()
  local userData = loadData(USER_DATA_FILE, USER_FILE_LOCATION);
  local settings = {};
  
  settings.music = (userData["music_volume"] or 1);
  settings.sound = (userData["sound_volume"] or 1);
  
  return settings;
end

function saveObjectPlacement(listObjects, saveFile)
  local dir = system.DocumentsDirectory;
  local data = {};
  if (listObjects == nil) then return; end
  
  data["items"] = {};
  
  for i = 1, #listObjects do
    local object = listObjects[i];
    local objectData = 
    {
      name = object.name,
      filename = object.filename,
      width = (object.width * object.xScale),
      height = (object.height * object.yScale),
      rotation = object.rotation,
      xPos = object.x,
      yPos = object.y,
      type = object.type,
      dispatch = (object.dispatch..i),
    };
    
    table.insert(data["items"], objectData);
  end
  
  saveData(data, saveFile..".json", dir);

end

function loadObjectPlacement(strFilename)
  strFilename = (strFilename .. ".json");
  return loadData(strFilename, system.ResourceDirectory);
end
