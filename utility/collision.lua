module(..., package.seeall);

local widget = require("widget");
local factory = require("utility.factory");

local _W = display.contentWidth;
local _H = display.contentHeight;
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 768);
local yScale = (_H / 1136);

--delegates for efficiency
local rand = math.random;
local randSeed = math.randomseed;
local randTime = os.time;

randSeed(randTime());
rand(); rand(); rand();

--local function isTouchWithinBounds(touchX, touchY, xMin, xMax, yMin, yMax)
--	if ((touchX > xMin) and (touchX < xMax)) then
--		if ((touchY > yMin) and (touchY < yMax)) then
--			return true;
--		end
--	end
	
--	return false;
--end

function isColliding(object1, object2)
  if ((object2 == nil) or (object2.contentBounds == nil)) then return false; end
  
  if (object1.contentBounds.xMax < object2.contentBounds.xMin) then
    return false;
  elseif (object1.contentBounds.xMin > object2.contentBounds.xMax) then
    return false;
  elseif (object1.contentBounds.yMax < object2.contentBounds.yMin) then
    return false;
  elseif (object1.contentBounds.yMin > object2.contentBounds.yMax) then
    return false;
  end
  
  return true;
    
end

local function handleWaterAccumulation(listProblems, procedure)
  local currentWater = listProblems["water"];
  
  Runtime:dispatchEvent({name = "water_tool_sound"});
  
  local index;
  
  if (currentWater == nil) then
    index = 1;
    
  else
    index = currentWater.index + 1;
    
    if (index > 4) then return; end
    
    display.remove(listProblems["water"]);
  end
  
  currentWater = display.newImage("assets/images/tools/water"..index..".png");
  currentWater.xScale = xScale;  currentWater.yScale = yScale;
  currentWater.x = _w;
  currentWater.y = (_H * 0.68);
  currentWater.index = index;
  listProblems["gas"].parent:insert(currentWater);
  listProblems["water"] = currentWater;
  
  if (procedure == "procedure3") then
    listProblems["water"].xScale = (listProblems["water"].xScale * 2);
    listProblems["water"].yScale = (listProblems["water"].yScale * 2);
    listProblems["water"].x = _w;
    listProblems["water"].y = (_H * 0.55);
  end
  
end

local function handleWaterSprayer(selectedTool, listProblems, procedure)
  if (isColliding(selectedTool.hitbox, listProblems["gas"]) == false) then return; end
  
  listProblems["gas"].alpha = (listProblems["gas"].alpha - 0.25);
  
  if (listProblems["gas"].alpha <= 0) then
    listProblems["gas"].alpha = 0;
    listProblems["gas"].destroyed = true;
  end
  
  handleWaterAccumulation(listProblems, procedure);
end

local function handleWaterSuction(selectedTool, listProblems, procedure)
  if (isColliding(selectedTool.hitbox, listProblems["water"]) == false) then return; end
  
  local currentWater = listProblems["water"];
  
  local index = currentWater.index;
  index = (index - 1);
  
  display.remove(listProblems["water"]);
  
  if (index == 0) then 
    listProblems["water"].destroyed = true;
    listProblems["water"].index = 0;
    return; 
  end
  
  currentWater = display.newImage("assets/images/tools/water"..index..".png");
  currentWater.xScale = xScale;  currentWater.yScale = yScale;
  currentWater.x = _w;
  currentWater.y = (_H * 0.68);
  currentWater.index = index;
  listProblems["gas"].parent:insert(currentWater);
  listProblems["water"] = currentWater;
  
  if (procedure == "procedure3") then
    listProblems["water"].xScale = (listProblems["water"].xScale * 2);
    listProblems["water"].yScale = (listProblems["water"].yScale * 2);
    listProblems["water"].x = _w;
    listProblems["water"].y = (_H * 0.55);
  end
end

local function fadeOutProblem(problem)
  transition.to(problem, {time = 500, alpha = 0});
end

local function handleSyringe(selectedTool, listProblems)
  for i,v in pairs(listProblems) do
    
    if ((v.tool == "syringe") and (v.destroyed ~= true) and (isColliding(selectedTool.hitbox,v) == true) and 
        (selectedTool.firstState.isVisible == true)) then
      v.destroyed = true;
      selectedTool.firstState.isVisible = false;
      selectedTool.secondState.isVisible = true;
      Runtime:dispatchEvent({name = "syringe_tool_sound"});
      fadeOutProblem(v);
      return;
    end
  end
end

local function handleSwab(selectedTool, listProblems)
  for i,v in pairs(listProblems) do
    
    if ((v.tool == "swab") and (v.destroyed ~= true) and (isColliding(selectedTool.hitbox,v) == true)) then
      v.alpha = (v.alpha - 0.25);
      
      Runtime:dispatchEvent({name = "swab_tool_sound"});
      
      if (v.alpha <= 0) then
        v.alpha = 0;
        v.destroyed = true;
        
      end
      
      return;
    end
  end
end

local function handlePicker(selectedTool, listProblems, procedure)
  for i,v in pairs(listProblems) do
    
    if ((v.tool == "picker") and (v.destroyed ~= true) and (isColliding(selectedTool.hitbox,v) == true) and 
        (selectedTool.firstState.isVisible == true)) then
      Runtime:dispatchEvent({name = "picker_tool_sound"});
      selectedTool.firstState.isVisible = false;
      selectedTool.secondState.isVisible = true;
      v.destroyed = true;
      v.alpha = 0;
      
      local image = factory.createNewImage(image,v,v.filename);
      
      if (procedure == "procedure3") then
        image.xScale = (image.xScale * 0.5);
        image.yScale = (image.yScale * 0.5);
      end
      
      selectedTool:insert(image);
      selectedTool.problem = image;
      image.x = selectedTool.hitbox.x;
      image.y = selectedTool.hitbox.y;
      
      Runtime:dispatchEvent({name = "open_mini_try", tool = selectedTool});
      
      return;
    end
  end
end

local function checkToolMiniTrayCollision(selectedTool, miniTray, procedure)
  if (isColliding(selectedTool.hitbox,miniTray.hitbox) == false) then return; end
  
  local problem = selectedTool.problem;
  
  if (problem == nil) then return; end
  
  Runtime:dispatchEvent({name = "drop_tool_sound"});
  
  local image = factory.createNewImage(image, problem, problem.filename);
  
  if (procedure == "procedure3") then
    image.xScale = (image.xScale * 0.5);
    image.yScale = (image.yScale * 0.5);
  end
  
  display.remove(problem);
  selectedTool.problem = nil;
  
  selectedTool.firstState.isVisible = true;
  selectedTool.secondState.isVisible = false;
  
  miniTray:insert(image);
  
  image.x = (selectedTool.hitbox.contentBounds.xMin-370);
  image.y = selectedTool.hitbox.contentBounds.yMin;
  
  selectedTool:toFront();
end

function placeRandomInMiniTray(selectedTool, miniTray)
  local problem = selectedTool.problem;
  
  local image = factory.createNewImage(image, problem, problem.filename);
  
  display.remove(problem);
  selectedTool.problem = nil;
  
  selectedTool.firstState.isVisible = true;
  selectedTool.secondState.isVisible = false;
  
  miniTray:insert(image);
  
  image.x = rand(miniTray.hitbox.contentBounds.xMin-370,miniTray.hitbox.contentBounds.xMax-370);
  image.y = rand(miniTray.hitbox.contentBounds.yMin,miniTray.hitbox.contentBounds.yMax);
  
  
  selectedTool:toFront();
end

local function handleHeater(selectedTool, listProblems)
  for i,v in pairs(listProblems) do
    
    if ((v.tool == "heater") and (v.destroyed ~= true) and (isColliding(selectedTool.hitbox,v) == true)) then
      v.alpha = (v.alpha - 0.2);
      
      if (v.alpha <= 0) then
        v.alpha = 0;
        v.destroyed = true;
      end
      
      return;
    end
  end
end

local function fadeOintmentGel(image)
  timer.performWithDelay(1500, function()
    transition.to(image, {time = 1000, alpha = 0, onComplete = function()
      display.remove(image);
    end});
    
  end);
  
end

local function handleOintment(selectedTool, listProblems, procedure)
  for i,v in pairs(listProblems) do
    
    if ((v.tool == "ointment") and (v.destroyed ~= true) and (isColliding(selectedTool.hitbox,v) == true)) then
      v.alpha = 0;
      v.destroyed = true;
      
      Runtime:dispatchEvent({name = "ointment_tool_sound"});
      
      local image = display.newImage("assets/images/tools/ointmentgel.png");
      image.xScale = (xScale * 2);
      image.yScale = (yScale * 2);
      
      if (v.filename == "assets/images/throatparts/bruise1.png") then
        image.rotation = -60;
      else
        image.rotation = 60;
      end
      
      image.x = v.x;
      image.y = v.y;
      v.parent:insert(image);
      
      fadeOintmentGel(image);
      
      return;
    end
  end
end

function checkCollision(selectedTool, listProblems, miniTray, procedure)
  if (selectedTool == nil) then return; end
  
  if (selectedTool.name == "water_sprayer") then
    handleWaterSprayer(selectedTool, listProblems, procedure);
  elseif (selectedTool.name == "water_suction") then
    handleWaterSuction(selectedTool, listProblems, procedure);
  elseif (selectedTool.name == "syringe") then
    handleSyringe(selectedTool, listProblems);
  elseif (selectedTool.name == "swab") then
    handleSwab(selectedTool, listProblems);
  elseif (selectedTool.name == "picker") then
    handlePicker(selectedTool, listProblems, procedure);
  elseif (selectedTool.name == "heater") then
    handleHeater(selectedTool, listProblems);
  elseif (selectedTool.name == "ointment") then
    handleOintment(selectedTool, listProblems, procedure);  
  end
  
  if (miniTray.isOpen == true) then
    checkToolMiniTrayCollision(selectedTool, miniTray, procedure);
  end
end
