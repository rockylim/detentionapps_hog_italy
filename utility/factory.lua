module(..., package.seeall);

local widget = require("widget");

local _W = display.contentWidth;
local _H = display.contentHeight;
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 1136);
local yScale = (_H / 768);

--local function sortLayerInTable(layer1, layer2)
--  return layer1 < layer2;
--end

--local function insert(self, obj)
--  --insert based on layer#
--  self:insert(obj);
--end

--local function isTouchWithinBounds(touchX, touchY, xMin, xMax, yMin, yMax)
--	if ((touchX > xMin) and (touchX < xMax)) then
--		if ((touchY > yMin) and (touchY < yMax)) then
--			return true;
--		end
--	end
	
--	return false;
--end

function newGroup()
  local group = display.newGroup();
  
  group.type = "displayGroup";
  
  return group;
end

local function inputNet(e)
  Runtime:dispatchEvent({name = "inputNetTouched", phase = e.phase});
  if (e.phase == "began") then
    display.getCurrentStage():setFocus(e.target);
  elseif (e.phase == "ended") then
    display.getCurrentStage():setFocus(nil);
  end
  
  return true;
end

local function oneStateButtonTouch(e)
  Runtime:dispatchEvent({name = e.target.dispatch, target = e.target, event = e});
  print(e.target.dispatch);
  return true;
end

local function twoStateButtonTouch(e)
  Runtime:dispatchEvent({name = e.target.dispatch});
  print(e.target.dispatch);
end

local function toggle(e)
  local object = e.target;
  print(object.dispatch);
  if (object.toggleOff.isVisible == false) then
    --turn off
    object.toggleOff.isVisible = true;
    object.toggleOn.isVisible = false;
    Runtime:dispatchEvent({name = object.dispatch, toggle = "off",target = e.target});
  else
    --turn om
    object.toggleOff.isVisible = false;
    object.toggleOn.isVisible = true;
    Runtime:dispatchEvent({name = object.dispatch, toggle = "on",target = e.target});
  end
  return true;
end

function createNewImage(object, data, filename)
  object = display.newImage(filename);
    
  object.width = (data.width or object.width);
  object.height = (data.height or object.height);
  object.xScale = xScale;
  object.yScale = yScale;
  
  if (data.xScale) then
    object.xScale = data.xScale;
    object.yScale = data.yScale;
  end
  
  object.x = data.xPos;
  object.y = data.yPos;
  object.originalX = object.x;
  object.originalY = object.y;
  object.filename = filename;
  
  if (data.rotation) then
    object.rotation = data.rotation;
  end
  
  if (data.color) then
    object:setFillColor(unpack(data.color));
  end
  
  return object;
end

function newText(str, size, font, width, height)
  local defaultFont;
    
  if (_deviceOS == "Android") then
    defaultFont = "Arial";
  else
    defaultFont = "Arial";
  end
  
  --local font = native.systemFont;
  if ((font ~= "default") and (font ~= nil)) then
    defaultFont = font;
  end
  
  local object;
  
  if (width ~= nil) then
    object = display.newText(str, 0, 0, width,height, defaultFont, size);
  else
    object = display.newText(str, 0, 0, defaultFont, size);
  end
  
  
  
  return object;
end

local function createSpriteSheet(data)
  
end

local currentDragX,currentDragY;

local function dragObject(e)
  if (e.target.locked == true) then 
    if (e.phase == "ended") then
      Runtime:dispatchEvent({name = "buy_game_unlock"});
    end
    
    return; 
  end
  
  if (e.phase == "began") then
    display.getCurrentStage():setFocus(e.target);
    e.target.isFocus = true;
    currentDragX = e.x;
    currentDragY = e.y;
    Runtime:dispatchEvent({name = "began_drag", target = e.target});
    
  elseif ((e.phase == "moved") and (e.target.isFocus == true)) then  
    local deltaX = (e.x - currentDragX);
    local deltaY = (e.y - currentDragY);
    
    e.target.x = (e.target.x + deltaX);
    e.target.y = (e.target.y + deltaY);
    
    currentDragX = e.x;
    currentDragY = e.y;
    
    Runtime:dispatchEvent({name = "check_collision", target = e.target});
    
  elseif ((e.phase == "ended") and (e.target.isFocus == true)) then
    Runtime:dispatchEvent({name = "stop_drag", target = e.target});
    display.getCurrentStage():setFocus(nil);
    e.target.isFocus = nil;
  end
  
  return true;
end

local function handleCloneMove(e)
  if (e.phase == "moved") then
    local deltaX = (e.x - currentDragX);
    local deltaY = (e.y - currentDragY);
    
    e.target.x = (e.target.x + deltaX);
    e.target.y = (e.target.y + deltaY);
    
    currentDragX = e.x;
    currentDragY = e.y;
    
    Runtime:dispatchEvent({name = "check_collision", target = e.target});
  elseif (e.phase == "ended") then
    Runtime:dispatchEvent({name = "stop_drag", target = e.target});
    display.getCurrentStage():setFocus(nil);
    e.target.isFocus = nil;
    display.remove(e.target);
  end    
  return true;
end

local function spawnDragObject(e)
  if (e.target.locked == true) then 
    if (e.phase == "ended") then
      Runtime:dispatchEvent({name = "buy_game_unlock"});
      Runtime:dispatchEvent({name = "button_sound"});
    end
    
    return; 
  end
  
  if (e.phase == "began") then
    local group = newGroup();
    local image = createNewImage(image,e.target.cloneData,e.target.cloneData.filename);
    local hitbox = display.newRect(0,0,e.target.hitboxData.width, e.target.hitboxData.height);
    hitbox.x = e.target.hitboxData.xPos;
    hitbox.y = e.target.hitboxData.yPos;
    hitbox.alpha = 0.6;
    
    group.name = e.target.cloneData.name;
    
    group:insert(image);
    group:insert(hitbox);
    group.hitbox = hitbox;
    group:setReferencePoint(display.CenterReferencePoint);
    
    display.getCurrentStage():setFocus(group);
    group.isFocus = true;
    group:addEventListener("touch", handleCloneMove);
    
    group.x = e.x;
    group.y = e.y;
    
    currentDragX = group.x;
    currentDragY = group.y;
    Runtime:dispatchEvent({name = "began_drag", target = group});
  end
end

local function createDraggableSpawner(data)
  local object = newGroup();
  local item = createNewImage(item, data, data.filename);
  
  object.dispatch = data.dispatch;
  object:addEventListener("touch", spawnDragObject);
  object:insert(item);
  object:setReferencePoint(display.CenterReferencePoint);
  item.name = data.name;
  item.type = data.type;
  item.dispatch = data.dispatch;
  
  object.hitboxData = data.hitbox;
  object.cloneData = data.clone;
  
  return object;
end

local function createHitboxObject(data)
  local image = createNewImage(image, data, data.filename);
  local hitbox;
  
  
  local hitboxData = data.hitbox;
  hitbox = display.newRect(0,0,hitboxData.width, hitboxData.height);
  hitbox.alpha = 0.4;
  hitbox.x = hitboxData.xPos;
  hitbox.y = hitboxData.yPos;
  hitbox.dispatch = hitboxData.dispatch;
  hitbox.isVisible = false;
  
  local group = display.newGroup();
  group:insert(image);
  group:insert(hitbox);
  group:setReferencePoint(display.CenterReferencePoint);
  
  group.image = image;
  group.hitbox = hitbox;
  
  return group;
end

function createObject(data)
  local object;
  
  if (data.type == "image") then
    object = createNewImage(object, data, data.filename);
    object.tool = data.tool;
    
  elseif (data.type == "text") then
    object = newText(data.text, data.size, data.font, data.width, data.height);
    object:setTextColor(unpack(data.color));
    object.x = data.xPos;
    object.y = data.yPos;
  elseif (data.type == "rect") then
    object = display.newRect(0,0,data.width, data.height);
    object.xScale = xScale;
    object.yScale = yScale;
    object.x = data.xPos;
    object.y = data.yPos;
    object:setFillColor(unpack(data.color));
  
  elseif (data.type == "input_net") then
    object = display.newRect(0,0,data.width, data.height);
    object.xScale = xScale;
    object.yScale = yScale;
    object.x = data.xPos;
    object.y = data.yPos;
    --object:setFillColor(unpack(data.color));
    --object.alpha = (data.alpha / 100);
    object.alpha = 0.01;
    object:addEventListener("touch", inputNet);
    object:addEventListener("tap", inputNet);
  
  elseif (data.type == "input_net_image") then
    object = createNewImage(object, data, data.filename);
    
    object:addEventListener("touch", inputNet);
    object:addEventListener("tap", inputNet);
    
  elseif (data.type == "one_state_button") then
    object = createNewImage(object, data, data.filename);
    object:addEventListener("tap", oneStateButtonTouch);
    object:addEventListener("touch", inputNet);
    object.dispatch = data.dispatch;
    if (data.name == "scope") then
      object.yScale = -object.yScale;
    end
    
  elseif (data.type == "two_state_button") then
    object = widget.newButton
    {
        width = data.width,
        height = data.height,
        defaultFile = data.untouchedFile,
        overFile = data.touchedFile,
        onRelease = twoStateButtonTouch,
    }
    
    object.xScale = xScale;
    object.yScale = yScale;
    object.x = data.xPos;
    object.y = data.yPos;
    
    if (data.rotation) then
      object.rotation = data.rotation;
    end
    
    object.dispatch = data.dispatch;
    
  elseif (data.type == "toggle_button") then
    object = newGroup();
    object.dispatch = data.dispatch;
    
    local toggleOff = createNewImage(toggleOff, data, data.toggleOff);
    local toggleOn = createNewImage(toggleOn, data, data.toggleOn);
    
    object:insert(toggleOff);
    object:insert(toggleOn);
    
    toggleOff.isVisible = false;
    
    object.toggleOn = toggleOn;
    object.toggleOff = toggleOff;
    object:addEventListener("tap", toggle);
    
  elseif (data.type == "animation") then
    object = require("utility.animation");
    object = object:new();
    object:initialize(data);
  elseif (data.type == "rect_one_state_button") then
    object = display.newRect(0,0,data.width, data.height);
    object.xScale = xScale;
    object.yScale = yScale;
    object.x = data.xPos;
    object.y = data.yPos;
    object.alpha = 0.01;
    object.dispatch = data.dispatch;
    object:addEventListener("tap", oneStateButtonTouch);
  elseif (data.type == "forged_number") then
    object = forgeDigitsFromString(data.filename, data.number, 81, 2,2);
    object.x,object.y = data.xPos + 30, data.yPos; --LEVEL NUMBERS
  elseif (data.type == "draggable") then
    object = newGroup();
    local item = createNewImage(item, data, data.filename);
    
    if ((data.name == "heater") or (data.name == "scope")) then
      item.yScale = -item.yScale;
    end
    
    object.dispatch = data.dispatch;
    object:addEventListener("touch", dragObject);
    item.name = data.name;
    item.type = data.type;
    item.dispatch = data.dispatch;
    
    local secondImage;
    
    if (data.filename2) then
      secondImage = createNewImage(image, data, data.filename2);
      secondImage.x = item.x;
      secondImage.y = item.y;
      secondImage.isVisible = false;
      object.firstState = item;
      object.secondState = secondImage;
    end
    
    if (data.drag_effect) then
      local effectData = data.drag_effect;
      
      local image = createNewImage(image,effectData, effectData.filename);
      image.isVisible = false;
      
      if (effectData.name == "heat") then
        image.yScale = -image.yScale;
      end
      
      object.onImage = image;
      object:insert(image);
    end
    
    if (data.hitbox) then
      local hitboxData = data.hitbox;
      local hitbox = display.newRect(0,0,hitboxData.width, hitboxData.height);
      hitbox.xScale = xScale;
      hitbox.yScale = yScale;
      hitbox.x = hitboxData.xPos;
      hitbox.y = hitboxData.yPos;
      hitbox.dispatch = hitboxData.dispatch;
      hitbox.alpha = 0;
      object.hitbox = hitbox;
      object:insert(item);
      
      if (secondImage) then
        object:insert(secondImage);
      end
      
      object:insert(hitbox);
    end
  elseif (data.type == "draggable_spawner") then
    object = createDraggableSpawner(data);

--    object:setReferencePoint(display.CenterReferencePoint);
  elseif (data.type == "hitbox_object") then
    object = createHitboxObject(data);
  end
  
  print(data.type);
  
  object.name = data.name;
  object.type = data.type;
  
  return object;
  
end

function forgeDigitsFromString(filename, num, spaceBetweenDigits, xScaleMod, yScaleMod)
  local strDigits = tostring(num);
  
  local counter = 1;
  local group = display.newGroup();
  
  for i = #strDigits, 1, -1  do
    local digitX = ((counter - 1) * -spaceBetweenDigits);
    local strDigit = string.sub(strDigits,i,i);
		local digit = display.newImage(filename .. strDigit .. ".png");
		
		digit.width = 61;
		digit.height = 69;
		digit.xScale = xScaleMod;
		digit.yScale = yScaleMod;
		
		digit.x = digitX;
		digit.y = 0;
    
    if (strDigit == "0") then
      --digit.y = 2;
    end
    
		group:insert(digit);
		counter = (counter + 1);
  end
  
  return group;
end

function forgeDigitsForCountdownTimer(filename, timeLeft, spaceBetweenDigits, xScale,yScale)
  local group = newGroup();
  
  timeLeft = (timeLeft / 1000);
  
  local minutes = math.floor(timeLeft / 60);
  local seconds = (timeLeft % 60);
  
  
  minutes = forgeDigitsFromString(filename, minutes, spaceBetweenDigits, xScale, yScale);
  
  if ((seconds >= 0) and (seconds < 10)) then
    seconds = tostring("0"..seconds);
  end
  
  seconds = forgeDigitsFromString(filename, seconds, spaceBetweenDigits, xScale, yScale);
  
  minutes.x = 0;
  minutes.y = 0;
  
  seconds.x = (minutes.x + (_W * 0.06));
  seconds.y = 0;
  
  local colon = newText(":", 36);
  
  colon.x = (_W * 0.02);
  colon.y = (_H * 0.0);
  colon:setTextColor(255,255,255);
  
  group:insert(minutes);
  group:insert(seconds);
  group:insert(colon);
  
  return group;
end


