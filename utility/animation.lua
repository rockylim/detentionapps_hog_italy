module(..., package.seeall);

local animation = {};

local _W = display.contentWidth
local _H = display.contentHeight
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 1136);
local yScale = (_H / 768);

animation.__index = animation;

function animation:new()
	 self = setmetatable({}, animation);
	 return self;
end

function animation:initialize(data)
  self.frames = {};
  self.loopCount = data.loopCount;
  self.width = data.width;
  self.height = data.height;
  self.numFrames = data.frames;
  self.name = (data.name or "animation");
  self.currentLoop = 1;
  
  self.group = display.newGroup();
  
  for i = 1, self.numFrames do
    self.frames[i] = display.newImage(data.filename .. i .. ".png");
    self.frames[i].width = data.width;
    self.frames[i].height = data.height;
    self.frames[i].xScale = xScale;
    self.frames[i].yScale = yScale;
    self.frames[i].isVisible = false;
    self.group:insert(self.frames[i]);
  end
  
  --self.group:setReferencePoint(display.CenterReferencePoint);
  self.group.anchorChildren = true;
  self.group.anchorX = 0.5;
  self.group.anchorY = 0.5;
  
  self.group.x = data.xPos;
  self.group.y = data.yPos;
  
  self.currentFrame = 0;
  self.active = false;
end

function animation:play(totalDelay)
  self.active = true;
  local _delay = (totalDelay / self.numFrames);
  self.currentFrame = (self.currentFrame + 1);
  self.frames[self.currentFrame].isVisible = true;
  
  self.handle = transition.to(self.frames[self.currentFrame], {time = _delay, onComplete = function(e)
    
    self.frames[self.currentFrame].isVisible = false;
    
    if ((self.currentFrame == self.numFrames) and (self.currentLoop == self.loopCount)) then
      print("endt");
      Runtime:dispatchEvent({name = "end_"..self.name, target = self});
      self.currentFrame = 0;
      
      if (self.loopCount ~= -1) then        
        self.active = false;
        self.currentLoop = 1;
        return;
      end
    elseif (self.currentFrame == self.numFrames) then
      self.currentLoop = (self.currentLoop + 1);
      self.currentFrame = 0;
    end
    
    self:play(totalDelay);
  end});
  
end

function animation:stop()
  if (self.handle) then
    transition.cancel(self.handle);
  end
  
  for i = 1, #self.frames do
    self.frames[i].isVisible = false;
  end
  
  self.currentFrame = 0;
  self.currentLoop = 1;
  self.active = false;
  
  
end

return animation;