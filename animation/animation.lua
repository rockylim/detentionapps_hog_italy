module(..., package.seeall);

local animation = display.newGroup();

local _W = display.contentWidth
local _H = display.contentHeight
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 1136);
local yScale = (_H / 768);

animation.__index = animation;

function animation:new()
	 self = setmetatable(display.newGroup(), animation);
	 return self;
end

function animation:initialize(data)
  self.frames = {};
  self.loopCount = (data.loopCount or -1);
  self.width = data.width;
  self.height = data.height;
  self.numFrames = data.frames;
  self.name = data.name;
  self.currentLoop = 1;
  
  for i = 1, self.numFrames do
    self.frames[i] = display.newImageRect(data.filename .. i .. ".png", data.width, data.height);
    
    self.frames[i].width = data.width;
    self.frames[i].height = data.height;
    self.frames[i].xScale = xScale;
    self.frames[i].yScale = yScale;
    self.frames[i].x = data.xPos;
    self.frames[i].y = data.yPos;
    self.frames[i].isVisible = false;
    self:insert(self.frames[i]);
  end
  
  --self:setReferencePoint(display.CenterReferencePoint);
  self.anchorChildren = true;
  self.anchorX = 0.5;
  self.anchorY = 0.5;
  
  self.currentFrame = 0;
  self.active = false;
end

function animation:play(totalDelay)
  self.active = true;
  local _delay = (totalDelay / self.numFrames);
  self.currentFrame = (self.currentFrame + 1);
  self.frames[self.currentFrame].isVisible = true;
  
  self.handle = transition.to(self.frames[self.currentFrame], {time = _delay, onComplete = function(e)
    
    self.frames[self.currentFrame].isVisible = false;
    
    if ((self.currentFrame == self.numFrames) and (self.currentLoop == self.loopCount)) then
      self.currentFrame = 0;
      
      if (self.loopCount ~= -1) then        
        Runtime:dispatchEvent({name = "end_"..self.name});
        self.active = false;
        self.currentLoop = 1;
        return;
      end
    elseif (self.currentFrame == self.numFrames) then
      self.currentLoop = (self.currentLoop + 1);
      self.currentFrame = 0;
    end
    
    self:play(totalDelay);
  end});
  
end

function animation:stop()
  if (self.handle) then
    transition.cancel(self.handle);
  end
  
  for i = 1, #self.frames do
    self.frames[i].isVisible = false;
  end
  
  self.currentFrame = 0;
  self.currentLoop = 1;
  self.active = false;
  
  
end

return animation;