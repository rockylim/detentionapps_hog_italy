module(..., package.seeall);

local _W = display.contentWidth;
local _H = display.contentHeight;
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 1136);
local yScale = (_H / 768);

local DELAY_FOUND_ANIMATION = 1000;

local assetManager = require("utility.assetManager");

local sparkleAnimation;

local handleAnimation;

local group;

function initialize(_group)
  group = display.newGroup();
  _group:insert(group);
end

local function endAnimation(e)
  display.remove(sparkleAnimation);
  Runtime:removeEventListener("end_found_animation", endAnimation);
end

function doAnimation(image)
  sparkleAnimation = assetManager.getAnimation("animation_found", group, image.x, image.y);
  
  sparkleAnimation.xScale = (100 / sparkleAnimation.width);
  sparkleAnimation.yScale = sparkleAnimation.xScale;
  
  sparkleAnimation:play(DELAY_FOUND_ANIMATION);
  
  Runtime:addEventListener("end_found_animation", endAnimation);
  
  transition.to(image, {time = 300, xScale = (-image.xScale), alpha = 0.65, onComplete = function(e)
    transition.to(image, {time = 300, xScale = -image.xScale, alpha = 0, onComplete = function(e)
      display.remove(image);    
    end});
  end});
  
end

function cancelAnimation()
  
end

function destroy()
  display.remove(sparkleAnimation);
  display.remove(group);
end
